<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class UserCourse extends Eloquent
{
    use HasFactory;

    protected $collection = 'user_courses'; //important

    protected $fillable = [
        'course_id',
        'user_id'
    ];

    protected $date = [
        'created_at', 
        'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
