<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use SoftDeletes, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $collection = 'users'; //important

    protected $fillable = [
        'name',
        'email',
        'role',
        'password',
    ];

    protected $date = [
        'created_at', 
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    public function userCourses()
    {
        return $this->hasMany(UserCourse::class);
    }

    public function assignment()
    {
        return $this->hasOne(Assignment::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}
