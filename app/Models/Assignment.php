<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Assignment extends Eloquent
{
    use HasFactory;

    protected $collection = 'assignments'; //important

    protected $fillable = [
        'post_id',
        'user_id',
        'description',
        'grade'
    ];

    protected $date = [
        'created_at', 
        'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function files()
    {
        return $this->hasMany(File::class);
    }
}
