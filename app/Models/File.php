<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class File extends Eloquent
{
    use HasFactory;

    protected $collection = 'files'; //important

    protected $fillable = [
        'post_id',
        'assignment_id',
        'name',
        'path',
        'extention'
    ];

    protected $date = [
        'created_at', 
        'updated_at'
    ];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function assignment()
    {
        return $this->belongsTo(Assignment::class);
    }
}
