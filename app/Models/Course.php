<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Course extends Eloquent
{
    use HasFactory;

    protected $collection = 'courses'; //important

    protected $fillable = [
        'code_course',
        'name',
        'description',
        'teacher_name'
    ];

    protected $date = [
        'created_at', 
        'updated_at'
    ];

    public function userCourses()
    {
        return $this->hasMany(UserCourse::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}
