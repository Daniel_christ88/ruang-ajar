<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\UserCourse;
use App\Models\Post;
use App\Models\Assignment;

class UserCourseController extends Controller
{
    public function index(Request $request) 
    {
        $user_courses = UserCourse::with(['user'])->with(['course'])->where('user_id', $request->id)->get();

        foreach ($user_courses as $user_course) {
            $posts = Post::where(['course_id' => $user_course['course']->_id, 'type' => 2])->get();

            $list_post = [];
            foreach ($posts as $post) {
                $assignment = Assignment::where(['post_id' => $post->_id, 'user_id' => $request->id])->first();
                if (is_null($assignment)) {
                    array_push($list_post, $post);
                }
            }
            $user_course->posts = $list_post;
        }

        return response()->json($user_courses, 200);
    }
}
