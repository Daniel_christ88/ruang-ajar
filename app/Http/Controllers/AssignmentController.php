<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Assignment;
use App\Models\File;
use App\Models\Post;
use Validator;

class AssignmentController extends Controller
{
    public function index(Request $request)
    {
        $assignments = Assignment::with(['user'])->where('post_id', $request->id)->get();

        if (count($assignments) != 0) {
            foreach ($assignments as $assignment) {
                $files = File::where('assignment_id', $assignment->_id)->get();
                $assignment->files = $files;
            }
        }

        return response()->json($assignments, 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'post_id' => 'required',
            'user_id' => 'required',
            'description' => 'sometimes'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $assignment = Assignment::create([
            'post_id' => $request->post_id,
            'user_id' => $request->user_id,
            'description' => $request->description,
            'grade' => 0
        ]);

        $files = $request->attachments;

        if ($request->hasFile('attachments')) {
            foreach ($files as $file) {
                $path = Storage::disk('public')->put("file/$request->user_id", $file);
                File::create([
                    'assignment_id' => $assignment->_id,
                    'name' => $file->getClientOriginalName(),
                    'path' => $path,
                    'extention' => $file->getClientOriginalExtension(),
                ]);
            }
        }

        return response()->json($assignment, 200);
    }

    public function update(Post $post, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'grades' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $assignments = Assignment::where('post_id', $post->_id)->get();
        
        for ($i = 0; $i < count($request->grades); $i++) {
            if (!is_null($request->grades[$i])) {
                $assignments[$i]->grade = $request->grades[$i];
                $assignments[$i]->save();
            }
        }

        return response()->json($assignments, 200);
    }

    public function destroy(Assignment $assignment, Request $request)
    {
        $files = File::where('assignment_id', $assignment->_id)->get();
        
        if (count($files) != 0) {
            foreach ($files as $file) {
                Storage::disk('public')->delete($file->path);
                $file->delete();
            }
        }

        $assignment->delete();
    }
}
