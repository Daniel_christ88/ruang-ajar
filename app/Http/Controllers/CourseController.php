<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\UserCourse;
use App\Models\User;
use Validator;
use Carbon\Carbon;

class CourseController extends Controller
{
    public function indexTutor(Request $request)
    {
        $user_courses = UserCourse::where('course_id', $request->id)->get();

        $tutors = [];
        foreach ($user_courses as $user_course) {
            $user = User::find($user_course->user_id);
            if ($user->role == 'teacher') {
                array_push($tutors, $user);
            }
        }

        return response()->json($tutors, 200);
    }

    public function show(Course $course)
    {
        return response()->json($course, 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'user' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $course = Course::create([
            'code_course' => Carbon::now()->timestamp,
            'name' => $request->name,
            'description' => $request->description,
            'teacher_name' => $request->user['name']
        ]);

        $user_course = UserCourse::create([
            'user_id' => $request->user['_id'],
            'course_id' => $course->_id
        ]);

        return response()->json($user_course, 200);
    }

    public function joinCourse(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code_course' => 'required',
            'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $course = Course::where('code_course', intval($request->code_course))->first();

        $user_course = UserCourse::where(['course_id' => $course->id, 'user_id' => $request->user_id])->first();

        if (is_null($user_course)) {
            UserCourse::create([
                'user_id' => $request->user_id,
                'course_id' => $course->id
            ]);
        } else {
            return response()->json("Course is already exist!", 204);
        }

        return response()->json(null, 200);
    }

    public function addNewTutor(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'course_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where(['email' => $request->email, 'role' => 'teacher'])->first();

        if (is_null($user)) {
            return response()->json(null, 204);
        }

        $user_course = UserCourse::where(['user_id' => $user->id, 'course_id' => $request->course_id])->first();

        if (is_null($user_course)) {
            UserCourse::create([
                'user_id' => $user->id,
                'course_id' => $request->course_id
            ]);
            return response()->json(null, 200);
        } else {
            return response()->json(null, 203);
        }
    }
}
