<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Post;
use App\Models\File;
use App\Models\User;
use App\Models\Course;
use App\Models\Assignment;
use Validator;

class PostController extends Controller
{
    public function indexPost(Request $request)
    {
        $post = Post::with(['user'])
            ->where('course_id', $request->id)
            ->orderBy('created_at', 'DESC')
            ->get();

        return response()->json($post, 200);
    }

    public function indexPostMaterial(Request $request)
    {
        $post_material = Post::with(['user'])
            ->where(['course_id' => $request->id, 'type' => 1])
            ->orderBy('created_at', 'DESC')
            ->get();

        return response()->json($post_material, 200);
    }

    public function indexAssignment(Request $request)
    {
        $posts = Post::with(['user'])->where(['course_id' => $request->id, 'type' => 2])->get();

        $list_post_assignment = [];
        foreach ($posts as $post) {
            $assignment = Assignment::where(['post_id' => $post->_id, 'user_id' => $request->user_id])->first();
            if (is_null($assignment)) {
                array_push($list_post_assignment, $post);
            }
        }

        return response()->json($list_post_assignment, 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'course_id' => 'required',
            'user_id' => 'required',
            'title' => 'required',
            'content' => 'required',
            'type' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $post = Post::create([
            'course_id' => $request->course_id,
            'user_id' => $request->user_id,
            'title' => $request->title,
            'content' => $request->content,
            'type' => intval($request->type)
        ]);
        
        $files = $request->attachments;

        if ($request->hasFile('attachments')) {
            foreach ($files as $file) {
                $path = Storage::disk('public')->put("file/$request->user_id", $file);
                File::create([
                    'post_id' => $post->_id,
                    'name' => $file->getClientOriginalName(),
                    'path' => $path,
                    'extention' => $file->getClientOriginalExtension(),
                ]);
            }
        }

        return response()->json($post);
    }

    public function show(Request $request)
    {
        // get data post
        $post = Post::with(['user'])->with(['course'])->find($request->post);
        
        // get file for this post
        $files = File::where('post_id', $request->post)->get();
        $post->files = $files;

        // get assignment for the post
        $assignment = Assignment::where(['post_id' => $request->post, 'user_id' => $request->user_id])->first();
        $post->assignment = $assignment;

        // get assignment files
        if (!is_null($assignment)) {
            $files = File::where('assignment_id', $assignment->_id)->get();
            $post->assignment->files = $files;
        }
        
        return response()->json($post, 200);
    }
}
