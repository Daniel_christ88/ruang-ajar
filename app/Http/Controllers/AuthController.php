<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

use Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'meta' => [
                    'status_code' => 422,
                    'message' => $validator->errors()
                ]
            ]);
        }

        $users = User::all();

        foreach ($users as $user) {
            if ($user->email == $request->email) {
                if (Hash::check($request->password, $user->password)) {
                    return response()->json([
                        'data' => $user,
                        'meta' => [
                            'status_code' => 200,
                            'message' => "Sukses Login!"
                        ]
                    ]);
                } else {
                    return response()->json([
                        'meta' => [
                            'status_code' => 204,
                            'message' => "Password not match!"
                        ]
                    ]);
                }
            } 
        }

        return response()->json([
            'meta' => [
                'status_code' => 204,
                'message' => "Email not found!"
            ]
        ]);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'role' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'meta' => [
                    'status_code' => 422,
                    'message' => $validator->errors()
                ]
            ]);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'role' => $request->role,
            'password' => Hash::make($request->password)
        ]);

        return response()->json([
            'data' => $user,
            'meta' => [
                'status_code' => 200,
                'message' => 'Register Success!'
            ]
        ]);
    }
}
