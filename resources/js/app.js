require('./bootstrap');

import Vue from 'vue';

// import dependecies tambahan
import VueAxios from 'vue-axios';
import Axios from 'axios';
import Buefy from 'buefy';
import router from './router/index';

import { ValidationProvider, extend } from "vee-validate";
import { ValidationObserver } from "vee-validate";
import { required, email } from "vee-validate/dist/rules";

Vue.component("ValidationProvider", ValidationProvider);
Vue.component("ValidationObserver", ValidationObserver);

extend("email", email);

extend("password", {
    validate: (value, { other }) => value === other,
    message: "The password confirmation does not match.",
    params: [{ name: "other", isTarget: true }]
});

extend("phone", {
    message: "Invalid phone number",
    validate(value) {
        return /^-?[\d.]+(?:e-?\d+)?$/.test(value);
    }
});

extend("required", {
    ...required,
    message: "This field is required"
});

import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(fas);

Vue.use(Buefy, {
    defaultIconComponent: FontAwesomeIcon,
    defaultIconPack: "fas"
});

Vue.config.productionTip = false;

Vue.use(VueAxios, Axios);
Vue.use(Buefy);

// import component
import App from '../views/vue/App.vue';

new Vue({
    el: '#app',
    router: router,
    render: h => h(App)
});

