const ID_USER = "user";

function setCookie(cvalue) {
    let d = new Date();
    d.setTime(d.getTime() + 3 * 24 * 60 * 60 * 1000); // 3 days
    let expires = "expires=" + d.toUTCString();
    document.cookie = ID_USER + "=" + JSON.stringify(cvalue) + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(";");
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == " ") {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
export const isAuthenticated = () => {
    if (getCookie(ID_USER) != '') {
        return true;
    } else {
        return false;
    }
};

export const getUser = () => {
    return getCookie(ID_USER);
};

export const saveUser = user => {
    setCookie(user);
};

export const destroyUser = () => {
    document.cookie = ID_USER + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
};

export default {
    isAuthenticated,
    getUser,
    saveUser,
    destroyUser
};