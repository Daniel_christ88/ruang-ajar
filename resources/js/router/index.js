import Vue from "vue";
import VueRouter from "vue-router";
import JWTService from "../helpers/jwt.service";
Vue.use(VueRouter);

// components
import Home from "../../views/vue/Home.vue";
import Login from "../../views/vue/auth/Login.vue";
import Register from "../../views/vue/auth/Register.vue";
import Course from "../../views/vue/Course.vue";
import FormPost from "../../views/vue/FormPost.vue";
import Post from "../../views/vue/Post.vue";

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            name: 'home',
            path: '/',
            component: Home
        },
        {
            name: 'login',
            path: '/login',
            component: Login
        },
        {
            name: 'register',
            path: '/register',
            component: Register
        },
        {
            name: 'course',
            path: '/course/:courseId',
            component: Course
        },
        {
            name: 'createPost',
            path: '/post/create/:courseId',
            component: FormPost
        },
        {
            name: 'formPost',
            path: '/post/:postId',
            component: Post
        },
    ]
});

router.beforeEach((to, from, next) => {
    if (JWTService.isAuthenticated() == false && to.name !== "login") {
        if (to.name == 'register') {
            next();
        } else {
            next("login");
        }
    } else if ((to.name == "login" || to.name == "register") && JWTService.isAuthenticated()) {
        next("/");
    } else {
        next();
    }
});

export default router;