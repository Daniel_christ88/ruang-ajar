<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'  => 'Teacher',
            'email' => 'teacher@gmail.com',
            'role' => 'teacher',
            'password' => Hash::make('teacher')
        ]);

        User::create([
            'name'  => 'Benedict Reydo',
            'email' => 'benedict@gmail.com',
            'role' => 'student',
            'password' => Hash::make('student')
        ]);
        
        User::create([
            'name'  => 'Andreas Aditya',
            'email' => 'andreas@gmail.com',
            'role' => 'student',
            'password' => Hash::make('student')
        ]);

        User::create([
            'name'  => 'Daniel Christianto',
            'email' => 'daniel@gmail.com',
            'role' => 'student',
            'password' => Hash::make('student')
        ]);

        User::create([
            'name'  => 'Alexander Tri Handoyo',
            'email' => 'alexander@gmail.com',
            'role' => 'student',
            'password' => Hash::make('student')
        ]);

        User::create([
            'name'  => 'Chris Christian',
            'email' => 'chris@gmail.com',
            'role' => 'student',
            'password' => Hash::make('student')
        ]);
    }
}
