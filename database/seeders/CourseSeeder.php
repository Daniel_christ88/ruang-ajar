<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Course;
use App\Models\UserCourse;
use App\Models\User;
use Carbon\Carbon;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::get();

        $course = Course::create([
            'code_course' => Carbon::now()->timestamp,
            'name' => 'Artificial Intelegence',
            'description' => 'IF-789 ITHB 2020/2021',
            'teacher_name' => 'Teacher'
        ]);

        $course2 = Course::create([
            'code_course' => Carbon::now()->timestamp,
            'name' => 'Machine Learning',
            'description' => 'IF-999 ITB 2020/2021',
            'teacher_name' => 'Teacher'
        ]);

        for ($i = 0; $i < count($user); $i++) {
            if ($user[$i]->role == 'teacher') {
                UserCourse::create([
                    'user_id' => $user[$i]->_id,
                    'course_id' => $course->_id
                ]);
                UserCourse::create([
                    'user_id' => $user[$i]->_id,
                    'course_id' => $course2->_id
                ]);
            } else if ($user[$i]->role == 'student') {
                UserCourse::create([
                    'user_id' => $user[$i]->_id,
                    'course_id' => $course->_id
                ]);
                UserCourse::create([
                    'user_id' => $user[$i]->_id,
                    'course_id' => $course2->_id
                ]);
            }
        }
    }
}
