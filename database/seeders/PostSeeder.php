<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Course;
use App\Models\Post;
use App\Models\User;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam varius eros elit, in aliquet diam consectetur pretium. Aenean nec libero vitae magna dapibus imperdiet non non diam. Duis vitae neque egestas, cursus velit id, mattis turpis. Etiam in elit varius felis lacinia fermentum. Nam sit amet sodales libero, ac aliquam turpis. Integer consectetur quis nisi sit amet ultricies. Nam rhoncus tincidunt felis, vel dictum tellus mattis vel. Fusce eget tortor augue.

        Cras massa quam, euismod non pellentesque sed, fermentum id lacus. Donec imperdiet sollicitudin purus eu faucibus. In nec semper magna. Nunc ut varius urna. Donec vehicula, lectus eu blandit pharetra, sapien sem egestas lectus, molestie efficitur metus lacus in mi. Quisque ut orci orci. Phasellus auctor cursus sapien. Sed sed nisi nisi. Morbi malesuada massa sit amet commodo scelerisque.
        
        Donec quis leo ac tellus condimentum sollicitudin. Quisque vel egestas velit. Donec vel ullamcorper arcu. Pellentesque ut scelerisque enim. Quisque nec finibus magna, in rhoncus sapien. Etiam ante quam, consectetur vel tempor mattis, porttitor non massa. Praesent felis nunc, tincidunt ut ipsum nec, feugiat convallis diam. Praesent luctus arcu orci, vitae maximus ante eleifend vitae.
        
        Proin in diam lacus. Integer facilisis, ligula sit amet ultrices elementum, purus augue pretium dolor, id scelerisque nisl est in elit. Nullam mauris elit, ornare eget metus sed, placerat tristique metus. Fusce mattis erat id fermentum vulputate. Nullam ornare id tellus ut cursus. Etiam eu viverra nisi. Fusce tellus dui, imperdiet id dui sed, gravida pretium dolor. Sed sit amet felis a nulla molestie accumsan. Donec tristique euismod nisi eu auctor. Vivamus dapibus mi a ipsum consectetur, sit amet pretium nibh ultricies. Nunc felis nisi, pellentesque sed ligula sed, consectetur ultrices turpis. In quam tortor, fringilla non maximus at, rhoncus sodales risus.
        
        Donec consectetur, lacus faucibus accumsan dapibus, massa sapien efficitur arcu, vitae bibendum turpis mi et enim. Nam efficitur placerat vestibulum. Nunc a mi convallis, ornare nulla at, dictum lectus. Vivamus pulvinar, mi a hendrerit volutpat, ipsum est congue felis, ac bibendum ipsum magna et velit. Cras id scelerisque dui, id dictum diam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus bibendum metus ornare bibendum elementum. Duis dapibus convallis tortor id porttitor. Phasellus vel facilisis metus, non tristique est. Pellentesque rutrum metus in urna euismod egestas. Nam augue lectus, scelerisque nec est ut, sollicitudin scelerisque turpis. Praesent consequat fringilla aliquet.";

        $course = Course::get();

        $student = User::where(['role' => 'student', 'name' => 'Daniel Christianto'])->first();
        $teacher = User::where(['role' => 'teacher', 'name' => 'Teacher'])->first();

        for ($i = 0; $i < count($course); $i++) {
            Post::create([
                'course_id' => $course[$i]->_id,
                'user_id' => $teacher->_id,
                'title' => 'Week 1 - Breath First Search',
                'content' => $content,
                'type' => 1
            ]);

            Post::create([
                'course_id' => $course[$i]->_id,
                'user_id' => $teacher->_id,
                'title' => 'Week 2 - Deep First Search',
                'content' => $content,
                'type' => 1
            ]);

            $post = Post::create([
                'course_id' => $course[$i]->_id,
                'user_id' => $teacher->_id,
                'title' => 'Week 3 - Knowledge Based',
                'content' => $content,
                'type' => 1
            ]);

            Post::create([
                'course_id' => $course[$i]->_id,
                'user_id' => $student->_id,
                'title' => 'Share with another student',
                'content' => $content,
                'type' => 0
            ]);

            Post::create([
                'course_id' => $course[$i]->_id,
                'user_id' => $teacher->_id,
                'title' => 'Quiz',
                'content' => $content,
                'type' => 2
            ]);
        }
    }
}
