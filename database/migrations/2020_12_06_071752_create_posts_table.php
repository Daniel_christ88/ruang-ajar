<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function ($collection) {
            $collection->id();
            $collection->timestamps();
            $collection->string('course_id');
            $collection->string('user_id');
            $collection->string('title');
            $collection->longText('content');
            $collection->integer('type'); // 0: normal post , 1: post material, 2: post with submission
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
