<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'AuthController@login');
Route::post('/register', 'AuthController@register');

Route::group(['prefix' => 'user-course'], function ($router) {
    Route::get('/', 'UserCourseController@index');
});

Route::group(['prefix' => 'course'], function ($router) {
    Route::get('get-tutor', 'CourseController@indexTutor');
    Route::get('{course}/show', 'CourseController@show');
    Route::post('/', 'CourseController@store');
    Route::post('/join-course', 'CourseController@joinCourse');
    Route::post('/add-tutor', 'CourseController@addNewTutor');
});

Route::group(['prefix' => 'post'], function ($router) {
    Route::get('get-post', 'PostController@indexPost');
    Route::get('get-post-material', 'PostController@indexPostMaterial');
    Route::get('get-assignment', 'PostController@indexAssignment');
    Route::get('{post}/show', 'PostController@show');
    Route::post('/', 'PostController@store');
});

Route::group(['prefix' => 'assignment'], function ($router) {
    Route::get('/', 'AssignmentController@index');
    Route::post('/', 'AssignmentController@store');
    Route::post('{assignment}/delete', 'AssignmentController@destroy');
    Route::post('{post}/update', 'AssignmentController@update');
});
